import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TestClass {
    public static void main(String[] args) {
        getAnagrams("abcdefghijk");
    }

    private static List<String> getAnagrams(String string) {
        List<String> anagramasFinal = new ArrayList<>();

        if (string.length() == 1) {
            anagramasFinal.add(string);
        } else {
            for (int i = 0; i < string.length(); i++) {
                char c = string.charAt(i);

                StringBuilder stringBuilder = new StringBuilder(string);
                stringBuilder.delete(i, i + 1);

                List<String> anagramas = getAnagrams(stringBuilder.toString());
                anagramas = anagramas.stream().map(s -> Character.toString(c) + s).collect(Collectors.toList());

                anagramasFinal.addAll(anagramas);
            }
        }

        return anagramasFinal;
    }
}